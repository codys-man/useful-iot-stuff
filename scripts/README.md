###########################################################################################################
                                                                  hc_sr04.py usage
###########################################################################################################

- invocation command: `sudo nohup python hc_sr04.py gpio_pin_trigger:gpio_pin_echo ... gpio_pin_trigger:gpio_pin_echo
host_ip:host_ip username:username password:password &`
To get more info by while running the script add `--debug` arg to the base command:
`sudo nohup python hc_sr04.py gpio_pin_trigger:gpio_pin_echo ... gpio_pin_trigger:gpio_pin_echo
host_ip:host_ip --debug &`

- Per one sensor you have two gpio pins: gpio_pin_trigger and gpio_pin_echo. If you have only one sensor - just put one
pin_trigger:pin_echo pair. For one sensor the command will look like this:
`sudo nohup python hc_sr04.py your_pin_trigger_vale_here:your_pin_echo_value_here host_ip:host_ip --debug &`

- If you have two or more sensors - specify pin_trigger:pin_echo pairs for every sensor, separated by a space:
`sudo nohup python hc_sr04.py first_sensor_pin_trigger_vale_here:first_sensor_pin_echo_value_here
 second_sensor_pin_trigger_vale_here:second_sensor_pin_echo_value_here host_ip:host_ip --debug &`

*Note: the `nohup` operator and `&` mark in the end will de-touch the script's output from the console, thus it will run
in background. In order to get the scripts output to the console use the command without "nohup" and "&": 
`sudo python hc_sr04.py gpio_pin_trigger:gpio_pin_echo ... gpio_pin_trigger:gpio_pin_echo
host_ip:host_ip --debug`

*Note: the minimal Python version for this scripts is `Python 3.0`.



