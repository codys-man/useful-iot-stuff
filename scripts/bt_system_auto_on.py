#!/usr/bin/python3
import getopt
import os
import sys
import time

import bluetooth

authorized_addresses = []
sudo_password = 'raspberry'
global_shutdown_command = 'your script shut down command here!'
turn_on_command = 'your script turn on command here!'
alarm_on = False

HELP_MESSAGE = 'bt_system_auto_on.py [-a <device_address> | --address <device_address>] [-h | --help] [-u | --usage]'
# TODO:Implement comma separated values parsing to support multiple device address in one string parameter
MOBILE_PHONE_DEVICE_ADDRESS = None

# Arguments parsing
try:
    opts, args = getopt.getopt(sys.argv[1:], "uha:", ["address=", "help", "usage"])
except getopt.GetoptError:
    print(HELP_MESSAGE)
    sys.exit(2)

if len(opts) == 0:
    print(
        'There is no argument provided. \nUsage: {}'.format(HELP_MESSAGE))
    sys.exit(2)
for opt, arg in opts:
    if opt in ("-u", "--usage", "-h", "--help"):
        print(HELP_MESSAGE)
        sys.exit()
    elif opt in ("-a", "--address"):
        MOBILE_PHONE_DEVICE_ADDRESS = arg
        authorized_addresses.append(MOBILE_PHONE_DEVICE_ADDRESS)

while True:
    for address in authorized_addresses:
        print('\nLooking for the device with address {}'.format(MOBILE_PHONE_DEVICE_ADDRESS))
        result = bluetooth.lookup_name(MOBILE_PHONE_DEVICE_ADDRESS, timeout=5)
        print('The result is {}\n'.format(result))

        if result is not None and alarm_on:
            print('--------------------------------')
            print('Turing off the ultrasonic alarm!')
            print('--------------------------------')
            os.system('echo %s|sudo -S %s' % (sudo_password, global_shutdown_command))
            alarm_on = False
        elif result is None and not alarm_on:
            print('-------------------------------')
            print('Turing on the ultrasonic alarm!')
            print('-------------------------------')
            os.system('echo %s|sudo -S %s' % (sudo_password, turn_on_command))
            alarm_on = True
        # waiting to release system resources
        time.sleep(0.5)
