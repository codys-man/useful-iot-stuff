In this project, we prepared a batch of stuff that will be handy for your IoT development.

1. Under "server" you'll find a simple REST application that can handle events from your device/devices and save the 
results to the database.
It's packaged to a Docker container, so it can be easily deployed on a cloud service which will instantly make it 
reachable for your devices from anywhere.

2. Under "scripts" you'll find scripts samples for ultrasonic sensors control and processing of its measurements,
 a script for establishing a communication
 between Raspberry Pi and SIM808 GPS/GPRS modem and scripts for establishing communication between a mobile phone and
 Raspberry Pi via Bluetooth.

 Enjoy! 
