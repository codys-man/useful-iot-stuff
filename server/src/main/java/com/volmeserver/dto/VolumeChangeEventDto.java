package com.volmeserver.dto;

import com.volmeserver.domain.VolumeChangeEventEntity;

import java.util.List;

public interface VolumeChangeEventDto {
    VolumeChangeEventEntity save(VolumeChangeEventEntity event);

    VolumeChangeEventEntity findById(Long id);

    List<VolumeChangeEventEntity> findAll();

    List<VolumeChangeEventEntity> findAllById(List<Long> id);

    void deleteAll();

    void deleteById(Long id);

    boolean doesExist(Long id);
}
