package com.volmeserver.dto;

import com.volmeserver.domain.VolumeChangeEventEntity;
import com.volmeserver.exception.VolmeServerException;
import com.volmeserver.repositories.VolumeChangeEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VolumeChangeEventDtoImpl implements VolumeChangeEventDto {

    private VolumeChangeEventRepository repository;

    @Autowired
    public VolumeChangeEventDtoImpl(VolumeChangeEventRepository repository) {
        this.repository = repository;
    }

    @Override
    public VolumeChangeEventEntity save(VolumeChangeEventEntity event) {
        return repository.save(event);
    }

    @Override
    public VolumeChangeEventEntity findById(Long id) {
        if (repository.findById(id).isPresent()) {
            return repository.findById(id).get();
        } else throw new VolmeServerException(String.format("There is no entity with '%s' id!", id));
    }

    @Override
    public List<VolumeChangeEventEntity> findAll() {
        return (List<VolumeChangeEventEntity>) repository.findAll();
    }

    @Override
    public List<VolumeChangeEventEntity> findAllById(List<Long> id) {
        return (List<VolumeChangeEventEntity>) repository.findAllById(id);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public boolean doesExist(Long id) {
        return repository.existsById(id);
    }
}
