package com.volmeserver.model.response;

import java.util.Date;
import java.util.Objects;

public abstract class BaseServerResponse {

    public String getStatusDescription() {
        return statusDescription;
    }

    public Date getDate() {
        return date;
    }

    private String statusDescription;
    private Date date;

    public BaseServerResponse() {

    }

    public BaseServerResponse(String statusDescription, Date date) {
        this.statusDescription = statusDescription;
        this.date = date;
    }
    public BaseServerResponse(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public String toString() {
        if (date == null) {
            date = new Date();
        }
        return this.getClass().getName() + "{" +
                "statusDescription='" + statusDescription + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseServerResponse)) return false;
        BaseServerResponse that = (BaseServerResponse) o;
        return Objects.equals(statusDescription, that.statusDescription) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(statusDescription, date);
    }
}
