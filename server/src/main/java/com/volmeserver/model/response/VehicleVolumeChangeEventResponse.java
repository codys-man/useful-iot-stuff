package com.volmeserver.model.response;

import lombok.Data;

import java.util.Date;

@Data
public class VehicleVolumeChangeEventResponse extends BaseServerResponse {
    public VehicleVolumeChangeEventResponse(String statusDescription, Date date) {
        super(statusDescription, date);
    }
    public VehicleVolumeChangeEventResponse(String statusDescription) {
        super(statusDescription);
    }

    public VehicleVolumeChangeEventResponse() {
        super();
    }
}