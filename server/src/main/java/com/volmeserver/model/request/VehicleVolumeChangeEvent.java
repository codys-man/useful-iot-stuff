package com.volmeserver.model.request;

public class VehicleVolumeChangeEvent extends BaseEvent {

    public VehicleVolumeChangeEvent() {
    }

    public VehicleVolumeChangeEvent(String eventType, int deviceNumber, String location, String date) {
        super(eventType, deviceNumber, location, date);
    }

}