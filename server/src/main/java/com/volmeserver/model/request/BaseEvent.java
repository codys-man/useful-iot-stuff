package com.volmeserver.model.request;

import lombok.Data;

import java.util.Date;

@Data
public abstract class BaseEvent {

    private String eventType;
    private int deviceNumber;
    private String location;
    private Date date;

    public BaseEvent() {

    }

    public BaseEvent(String eventType, int deviceNumber, String location, String date) {
        this.eventType = eventType;
        this.date = new Date(Long.valueOf(date));
    }

    @Override
    public String toString() {

        return this.getClass().getSimpleName() + "{" +
                "eventType='" + eventType + '\'' +
                ", deviceNumber='" + deviceNumber +
                ", location='" + location + '\'' +
                ", date='" + date +
                '}';
    }
}