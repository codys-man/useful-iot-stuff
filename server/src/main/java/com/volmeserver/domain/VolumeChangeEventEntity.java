package com.volmeserver.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
public class VolumeChangeEventEntity {
    @Id
    @GeneratedValue
    private Long id;

    private String eventType;
    private int deviceNumber;
    private String location;
    private Date date;

    public VolumeChangeEventEntity() {

    }

    public VolumeChangeEventEntity(String eventType, int deviceNumber, String location, Date date) {
        this.eventType = eventType;
        this.deviceNumber = deviceNumber;
        this.location = location;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public int getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(int deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
