package com.volmeserver.domain;

import java.util.Date;
import java.util.Objects;

public class VolumeChangeEventEntityResponse {
    private Long id;
    private String eventType;
    private int deviceNumber;
    private String location;
    private Date date;

    public VolumeChangeEventEntityResponse(Long id, String eventType, int deviceNumber, String location, Date date) {
        this.id = id;
        this.eventType = eventType;
        this.deviceNumber = deviceNumber;
        this.location = location;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public String getEventType() {
        return eventType;
    }

    public int getDeviceNumber() {
        return deviceNumber;
    }

    public String getLocation() {
        return location;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "VolumeChangeEventEntityResponse{" +
                "id=" + id +
                ", eventType='" + eventType + '\'' +
                ", deviceNumber=" + deviceNumber +
                ", location='" + location + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VolumeChangeEventEntityResponse)) return false;
        VolumeChangeEventEntityResponse that = (VolumeChangeEventEntityResponse) o;
        return deviceNumber == that.deviceNumber &&
                eventType.equals(that.eventType) &&
                location.equals(that.location) &&
                date.equals(that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventType, deviceNumber, location, date);
    }
}
