package com.volmeserver.exception;

public class VolmeServerException extends RuntimeException {

    public VolmeServerException() {
        super();
    }

    public VolmeServerException(String message) {
        super(message);
    }

    public VolmeServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public VolmeServerException(Throwable cause) {
        super(cause);
    }
}
