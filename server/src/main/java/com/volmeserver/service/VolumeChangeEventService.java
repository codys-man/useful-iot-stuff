package com.volmeserver.service;

import com.volmeserver.domain.VolumeChangeEventEntity;
import com.volmeserver.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.model.request.BaseEvent;
import com.volmeserver.model.response.VehicleVolumeChangeEventResponse;

import java.util.List;

public interface VolumeChangeEventService {
    <S extends BaseEvent> void saveEvent(S event);

    VolumeChangeEventEntityResponse getEvent(Long id);

    List<VolumeChangeEventEntityResponse> getEvents(List<Long> ids);

    void deleteEvent(Long id);

    List<VolumeChangeEventEntityResponse> getAllEvents();

    void deleteAllEvents();

    VehicleVolumeChangeEventResponse doesExist(Long id);
}
