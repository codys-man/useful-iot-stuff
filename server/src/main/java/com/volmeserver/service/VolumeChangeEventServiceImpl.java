package com.volmeserver.service;

import com.volmeserver.domain.VolumeChangeEventEntity;
import com.volmeserver.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.dto.VolumeChangeEventDto;
import com.volmeserver.model.request.BaseEvent;
import com.volmeserver.model.response.VehicleVolumeChangeEventResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VolumeChangeEventServiceImpl implements VolumeChangeEventService {

    private VolumeChangeEventDto volumeChangeEventDto;

    @Autowired
    public VolumeChangeEventServiceImpl(VolumeChangeEventDto volumeChangeEventDto) {
        this.volumeChangeEventDto = volumeChangeEventDto;
    }

    @Override
    public <S extends BaseEvent> void saveEvent(S event) {
        VolumeChangeEventEntity entity = new VolumeChangeEventEntity(event.getEventType(), event.getDeviceNumber(),
                event.getLocation(), event.getDate());
        volumeChangeEventDto.save(entity);
    }

    @Override
    public VolumeChangeEventEntityResponse getEvent(Long id) {
        return convertToBaseType(volumeChangeEventDto.findById(id));
    }

    @Override
    public List<VolumeChangeEventEntityResponse> getEvents(List<Long> ids) {
        return volumeChangeEventDto.findAllById(ids).stream().map(this::convertToBaseType)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteEvent(Long id) {
        volumeChangeEventDto.deleteById(id);
    }

    @Override
    public void deleteAllEvents() {
        volumeChangeEventDto.deleteAll();
    }

    @Override
    public VehicleVolumeChangeEventResponse doesExist(Long id) {
        if (volumeChangeEventDto.doesExist(id)) {
            return new VehicleVolumeChangeEventResponse("The event exists in the database", new Date());
        } else return new VehicleVolumeChangeEventResponse("The event doesn't exist in the database",
                new Date());
    }

    @Override
    public List<VolumeChangeEventEntityResponse> getAllEvents() {
        return volumeChangeEventDto.findAll().stream().map(this::convertToBaseType).collect(Collectors.toList());
    }

    private VolumeChangeEventEntityResponse convertToBaseType(VolumeChangeEventEntity eventEntity) {
        return new VolumeChangeEventEntityResponse(eventEntity.getId(), eventEntity.getEventType(),
                eventEntity.getDeviceNumber(), eventEntity.getLocation(), eventEntity.getDate());

    }
}
