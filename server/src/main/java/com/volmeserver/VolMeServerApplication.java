package com.volmeserver;


import com.volmeserver.configuration.logging.VolmeServerLogger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VolMeServerApplication {

    public static void main(String[] args) {
        VolmeServerLogger.setup();
        SpringApplication.run(VolMeServerApplication.class, args);
    }
}