package com.volmeserver.controller;

import com.volmeserver.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.model.request.VehicleVolumeChangeEvent;
import com.volmeserver.model.response.BaseServerResponse;
import com.volmeserver.model.response.VehicleVolumeChangeEventResponse;
import com.volmeserver.service.VolumeChangeEventService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/event/volume-change-event")
public class EventsController {

    private static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private VolumeChangeEventService volumeChangeEventService;

    public EventsController(VolumeChangeEventService volumeChangeEventService) {
        this.volumeChangeEventService = volumeChangeEventService;
    }

    @PostMapping(value = "/", produces = APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseServerResponse> receiveVolumeChangeEvent(
            @RequestBody VehicleVolumeChangeEvent event) {
        logger.info(String.format("\n The following event has been received: %s\n", event.toString()));
        volumeChangeEventService.saveEvent(event);

        return new ResponseEntity<>(new VehicleVolumeChangeEventResponse(String.format(
                "\n The following event has been successfully saved: %s\n", event.toString())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE,
            consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<VolumeChangeEventEntityResponse> getEvent(@PathVariable Long id) {
        return new ResponseEntity<>(volumeChangeEventService.getEvent(id), HttpStatus.OK);
    }

    @GetMapping(value = "/events", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VolumeChangeEventEntityResponse>> getEvents(@RequestParam List<Long> ids) {
        return new ResponseEntity<>(volumeChangeEventService.getEvents(ids), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleVolumeChangeEventResponse> deleteEventById(@PathVariable Long id) {
        volumeChangeEventService.deleteEvent(id);
        return new ResponseEntity<>(new VehicleVolumeChangeEventResponse(
                "An event has been deleted successfully!", new Date()), HttpStatus.OK);
    }

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VolumeChangeEventEntityResponse>> getAllEvents() {
        return new ResponseEntity<>(volumeChangeEventService.getAllEvents(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleVolumeChangeEventResponse> deleteAllEvents() {
        volumeChangeEventService.deleteAllEvents();
        return new ResponseEntity<>(new VehicleVolumeChangeEventResponse(
                "All events have been deleted successfully!", new Date()), HttpStatus.OK);
    }

    @GetMapping(value = "/does-exist/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseServerResponse> doesExits(@PathVariable Long id) {
        return new ResponseEntity<>(volumeChangeEventService.doesExist(id), HttpStatus.OK);

    }

}