package com.volmeserver.service;

import com.volmeserver.domain.VolumeChangeEventEntity;
import com.volmeserver.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.dto.VolumeChangeEventDtoImpl;
import com.volmeserver.model.request.VehicleVolumeChangeEvent;
import com.volmeserver.model.response.VehicleVolumeChangeEventResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.Silent.class)
public class VolumeChangeEventServiceImplTest {

    @Mock
    private VolumeChangeEventDtoImpl eventDto;

    @InjectMocks
    private VolumeChangeEventServiceImpl volumeChangeEventService;

    @Test
    public void shouldSaveEvent() {
        //given
        VolumeChangeEventEntity entity = new VolumeChangeEventEntity("some-event", 1927,
                "Kyiv", new Date(1550058154424L));
        when(eventDto.save(entity)).thenReturn(entity);
        VehicleVolumeChangeEvent event = new VehicleVolumeChangeEvent(
                "some-event", 1927, "Kyiv", "1550058154424");
        //when
        volumeChangeEventService.saveEvent(event);
        //then
        //Provided comparision was implemented with 'any' matcher due to the internal organization of the
        // 'Mockito.verify() method and the current implementation of the VolumeChangeEventServiceImpl 'saveEvent()'
        // method: currently the VolumeChangeEvent entity is created inside 'saveEvent()' and 'verify()' doesn't use
        // 'equals()' method internally - it needs to have the exact same reference to and object to match a parameter.
        // Any suggestions to improve the implementation are welcomed!
        verify(eventDto, times(1)).save(any());
    }

    @Test
    public void shouldDeleteAllEvents() {
        //given
        doNothing().when(eventDto).deleteAll();
        //when
        volumeChangeEventService.deleteAllEvents();
        //than
        verify(eventDto, times(1)).deleteAll();
    }

    @Test
    public void shouldReturnEventById() {
        //given
        Long id = 1L;
        VolumeChangeEventEntity repositoryResponse = new VolumeChangeEventEntity("some_event",
                1927, "Kyiv", new Date(1550058154424L));
        repositoryResponse.setId(id);
        when(eventDto.findById(id)).thenReturn(repositoryResponse);
        VolumeChangeEventEntityResponse expected = new VolumeChangeEventEntityResponse(1L, "some_event",
                1927, "Kyiv", new Date(1550058154424L));
        //when
        VolumeChangeEventEntityResponse actual = volumeChangeEventService.getEvent(id);
        //then
        verify(eventDto, times(1)).findById(1L);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldReturnListOfEventsByIds() {
        //given
        List<Long> ids = Arrays.asList(1L, 2L, 1927L);
        List<VolumeChangeEventEntity> repositoryResponse = Arrays.asList(new VolumeChangeEventEntity(
                        "some-event", 1, "never-land", new Date(1550058154424L)),
                new VolumeChangeEventEntity("another-event", 2, "Kyiv",
                        new Date(1550058154424L)), new VolumeChangeEventEntity("and-another-one",
                        3, "somewhere", new Date(1550058154424L)));
        repositoryResponse.get(0).setId(1L);
        repositoryResponse.get(1).setId(2L);
        repositoryResponse.get(2).setId(1927L);
        when(eventDto.findAllById(ids)).thenReturn(repositoryResponse);
        List<VolumeChangeEventEntityResponse> expected = Arrays.asList(new VolumeChangeEventEntityResponse(
                        1L, "some-event", 1, "never-land", new Date(1550058154424L)),
                new VolumeChangeEventEntityResponse(2L, "another-event", 2,
                        "Kyiv", new Date(1550058154424L)), new VolumeChangeEventEntityResponse(1927L,
                        "and-another-one", 3, "somewhere", new Date(1550058154424L)));
        //when
        List<VolumeChangeEventEntityResponse> actual = volumeChangeEventService.getEvents(ids);
        //then
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldReturnAllEvents() {
        //given
        List<VolumeChangeEventEntity> repositoryResponse = Arrays.asList(new VolumeChangeEventEntity(
                        "some-event", 1, "never-land", new Date(1550058154424L)),
                new VolumeChangeEventEntity("another-event", 2, "Kyiv",
                        new Date(1550058154424L)), new VolumeChangeEventEntity("and-another-one",
                        3, "somewhere", new Date(1550058154424L)));
        repositoryResponse.get(0).setId(1L);
        repositoryResponse.get(1).setId(2L);
        repositoryResponse.get(2).setId(1927L);
        when(eventDto.findAll()).thenReturn(repositoryResponse);
        List<VolumeChangeEventEntityResponse> expected = Arrays.asList(new VolumeChangeEventEntityResponse(
                        1L, "some-event", 1, "never-land", new Date(1550058154424L)),
                new VolumeChangeEventEntityResponse(2L, "another-event", 2,
                        "Kyiv", new Date(1550058154424L)), new VolumeChangeEventEntityResponse(1927L,
                        "and-another-one", 3, "somewhere", new Date(1550058154424L)));
        //when
        List<VolumeChangeEventEntityResponse> actual = volumeChangeEventService.getAllEvents();
        //then
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldDeleteEventById() {
        //given
        Long id = 1927L;
        doNothing().when(eventDto).deleteById(id);
        //when
        volumeChangeEventService.deleteEvent(id);
        // then
        verify(eventDto, times(1)).deleteById(id);
    }

    @Test
    public void shouldValidateIfEventExists() {
        //given
        Long id = 1927L;
        when(eventDto.doesExist(id)).thenReturn(true);
        VehicleVolumeChangeEventResponse expected = new VehicleVolumeChangeEventResponse(
                "The event exists in the database", new Date());
        //when
        VehicleVolumeChangeEventResponse actual = volumeChangeEventService.doesExist(id);
        //then
        verify(eventDto, times(1)).doesExist(id);
        assertThat(expected, is(equalTo(actual)));
    }

}