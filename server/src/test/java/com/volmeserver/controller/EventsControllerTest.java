package com.volmeserver.controller;

import com.volmeserver.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.model.request.VehicleVolumeChangeEvent;
import com.volmeserver.model.response.BaseServerResponse;
import com.volmeserver.model.response.VehicleVolumeChangeEventResponse;
import com.volmeserver.service.VolumeChangeEventServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EventsControllerTest {
    @Mock
    private VolumeChangeEventServiceImpl volumeChangeEventService;

    @InjectMocks
    private EventsController controller;

    @Test
    public void shouldReceiveEventAndPassItToService() {
        //given
        VehicleVolumeChangeEvent event = new VehicleVolumeChangeEvent("some-event", 1,
                "some-location", "1550058154424");
        ResponseEntity<BaseServerResponse> expected = new ResponseEntity<>(new VehicleVolumeChangeEventResponse(
                "\n The following event has been successfully saved: %s\n" +
                        "VehicleVolumeChangeEvent{eventType='some-event', deviceNumber='0, " +
                        "location='null', date='Wed Feb 13 12:42:34 CET 2019}"), HttpStatus.OK);
        doNothing().when(volumeChangeEventService).saveEvent(event);
        //when
        ResponseEntity<BaseServerResponse> actual = controller.receiveVolumeChangeEvent(event);
        //then
        verify(volumeChangeEventService, times(1)).saveEvent(event);
        assertThat(expected, is(equalTo(actual)));
    }

    @Test
    public void shouldReturnEventResponseById() {
        //given
        Long id = 1L;
        ResponseEntity<VolumeChangeEventEntityResponse> expected = new ResponseEntity<>(
                new VolumeChangeEventEntityResponse(id, "some-event", 1, "some-location",
                        new Date(1550058154424L)), HttpStatus.OK);
        VolumeChangeEventEntityResponse entityResponse = new VolumeChangeEventEntityResponse(id, "some-event",
                1, "some-location", new Date(1550058154424L));
        when(volumeChangeEventService.getEvent(id)).thenReturn(entityResponse);
        //when
        ResponseEntity<VolumeChangeEventEntityResponse> actual = controller.getEvent(id);
        //then
        verify(volumeChangeEventService, times(1)).getEvent(id);
        assertThat(expected, is(equalTo(actual)));
    }

    @Test
    public void shouldReturnAllEvents() {
        //given
        ResponseEntity<List<VolumeChangeEventEntityResponse>> expected = new ResponseEntity<>(Arrays.asList(
                new VolumeChangeEventEntityResponse(1L, "some-event", 1,
                        "some-location", new Date(1550058154424L)), new VolumeChangeEventEntityResponse(
                        2L, "another-event", 2, "another-location",
                        new Date(1550058154424L)), new VolumeChangeEventEntityResponse(3L,
                        "and-another-one", 3, "and-another-one-location",
                        new Date(1550058154424L))), HttpStatus.OK);
        List<VolumeChangeEventEntityResponse> events = Arrays.asList(
                new VolumeChangeEventEntityResponse(1L, "some-event", 1,
                        "some-location", new Date(1550058154424L)), new VolumeChangeEventEntityResponse(
                        2L, "another-event", 2, "another-location",
                        new Date(1550058154424L)), new VolumeChangeEventEntityResponse(3L,
                        "and-another-one", 3, "and-another-one-location",
                        new Date(1550058154424L)));
        when(volumeChangeEventService.getAllEvents()).thenReturn(events);
        //when
        ResponseEntity<List<VolumeChangeEventEntityResponse>> actual = controller.getAllEvents();
        //then
        verify(volumeChangeEventService, times(1)).getAllEvents();
        assertThat(expected, is(equalTo(actual)));
    }

    @Test
    public void shouldDeleteEventById() {
        //given
        Long id = 1927L;
        doNothing().when(volumeChangeEventService).deleteEvent(id);
        //when
        controller.deleteEventById(id);
        //then
        verify(volumeChangeEventService, times(1)).deleteEvent(id);
    }


    @Test
    public void shouldDeleteAllEvents() {
        //given
        doNothing().when(volumeChangeEventService).deleteAllEvents();
        //when
        controller.deleteAllEvents();
        //then
        verify(volumeChangeEventService, times(1)).deleteAllEvents();
    }

    @Test
    public void doesExits() {
        //given
        Long id = 1L;
        VehicleVolumeChangeEventResponse serviceResponse = new VehicleVolumeChangeEventResponse("The " +
                "entity with the given id exits!", new Date(1550058154424L));
        ResponseEntity<VehicleVolumeChangeEventResponse> expected = new ResponseEntity<>(serviceResponse,
                HttpStatus.OK);
        when(volumeChangeEventService.doesExist(id)).thenReturn(serviceResponse);
        //when
        ResponseEntity<BaseServerResponse> actual = controller.doesExits(id);
        //then
        verify(volumeChangeEventService, times(1)).doesExist(id);
        assertThat(expected, is(equalTo(actual)));

    }
}