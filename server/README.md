To run the current version of the server application (by saying "current version" I mean, that the command below will
run a Docker image that I build and pushed to my DockerHub repo almost one year ago) you need to go to the "scripts"
directory under "server" (full path is `useful-iot-stuff\server\src\main\scripts>`) and execute `docker-compose up`.
Of course - you should have Docker installed on a machine to run it.
After executing this command Docker will download and run two images (for the server app and for MySQL database).

In other words - to start using this super-cool server application you need to run only one command
(`docker-compose up`) and all its REST endpoints will be available on port `8088`.

Each time during a build process Docker file and docker image get updated with the latest changes you made 
(`BuildDockerfile.groovy` is used for it).
The only thing you should do manually - is to push the image to your DockerHub.

In order to use images built by you - you need to modify `docker-compose.yml` correspondingly.